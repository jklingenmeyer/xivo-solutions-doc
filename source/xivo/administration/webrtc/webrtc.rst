******
WebRTC
******

General notes
=============

Current WebRC implementation requires to create user with one line configured for WebRTC. To have user with
both SIP ans WebRTC line is not supported.

.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Add `webrtc=yes` in Advanced Line options

.. figure:: webrtc_line.png
    :scale: 100%


Manual configuration of user with WebRTC line
=============================================

.. note:: Steps from previous section are fully sufficient. We recommend to follow them instead of manual configuration.

1. Create user

2. Optional: set codec to ulaw

.. figure:: webrtc_user_codecs.png
    :scale: 100%

3. Add line to user without any device

4. Configure Advanced Line options, so that it is usable with the softphone WebRTC

::

    avpf = yes
    call-limit = 1
    dtlsenable = yes ; Tell Asterisk to enable DTLS for this peer
    dtlsverify = no ; Tell Asterisk to not verify your DTLS certs
    dtlscertfile=/etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS cert file is
    dtlsprivatekey = /etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS private key is
    dtlssetup = actpass ; Tell Asterisk to use actpass SDP parameter when setting up DTLS
    encryption = yes
    force_avp = yes
    icesupport = yes
    transport = ws

.. figure:: webrtc_line_manual.png
    :scale: 100%
