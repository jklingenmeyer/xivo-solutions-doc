.. _ccinstallation:

************
Installation
************

This page describes how to install the XiVO CC.

It describes the installation with the debian package of the whole XiVO CC.

.. note::
  As a reference, the manual installation page is here :ref:`manual_configuration`.

.. warning::
  * the wizard **MUST** be passed on the XiVO PBX
  * XiVO PBX will be reconfigured during the installation and must be restarted.
    You may accept the automatic restart during the installation or you need to restart
    it manually later before starting the docker containers.
  * If you configure HA on XiVO, you have to reconfigure postgres for CC ...


Overview
========

The following components will be installed :

- XuC : outsourced CTI server providing telephony events, statistics and commands through a WebSocket
- XuC Management : supervision web pages based on the XuC
- Pack Reporting : statistic summaries stored in a PostgreSQL database
- Totem Support : near-real time statistics based on ElasticSearch_
- SpagoBI : BI suite with default statistic reports based on the Pack Reporting
- Recording Server : web server allowing to search recorded conversations
- Xuc Rights Management : permission provider used by XuC and Recording Server to manage the user rights

.. _ElasticSearch: https://www.elastic.co/


Prerequisites
=============

We will assume your **XiVO CC** server meets the following requirements:

- OS : Debian 8 (jessie), 64 bits.
- you have a XiVO PBX installed in a compatible version (basically the two components XiVO and XiVO CC have to be
  in the *same* version).
- the XiVO PBX is reachable on the network.
- the XiVO PBX **is setup** (wizard must be passed) with users, queues and agents, you must be able to place and answer calls.

For the rest of this page, we will make the following assumptions :

- the XiVO PBX has the IP 192.168.0.1
- some data (incoming calls, internal calls etc.) might be available on XiVO (otherwise, you will not see *anything* in the check-list_ below).
- the XiVO CC server has the IP 192.168.0.2
- the package xivo-recording is available on a custom Debian mirror. If this is not the case, you will need to skip the `apt-get install` commands and build the packages yourself.

XiVO PBX Restrictions and Limitations
=====================================

XiVO PBX enables a wide range of configuration, XiVO-CC is tested and validated with a number of
restriction concerning configurations of XiVO PBX:

General Configuration
---------------------
- Do not activate Contexts Separation in *xivo-ctid* Configuration
- Users deactivation is not supported

Queue Configuration
-------------------
- Queue ringing strategy should not be *Ring All*
- Do not use pause on one queue status, in queue advanced configuration, autopause should be No or All
- Do not activate Call a member already on (*Asterisk ringinuse*) on xivo queue advanced configuration

User And Agent Configuration
----------------------------
- All users and queues have to be in the same context
- Agent and Supervisors profiles should use the same Presence Group
- Agents and Phones should be in the same context for mobile agents
- Agents must not have a password in XiVO agent configuration page

Install from repository
=======================

The installation and configuration of XiVO CC (with its XiVO PBX part) is handled by the *xivocc-installer* package which is available in the repository.


Install process overview
------------------------

The install process consists of three parts:

#. The first part is to manually run the ``install-docker.sh`` script to install docker and docker compose.
#. The second part is the installation of *XiVO CC** itself.
#. The third part is to install the extra package for the recording.

The installation is automatic and you will be asked few questions during the process:

* When asked to generate a pair of authentication keys, leave the password field empty.
* Before copying the authentication keys, you will be prompted for the XiVO PBX root password.
* Enter IP addresses of XiVO PBX and XiVO CC.
* XiVO PBX must restart, the question will prompt you to restart during the process or to restart later.



Install Docker and Docker Compose
---------------------------------

.. note:: To be run on the XiVO CC server

On a fresh debian install you will probably need to install the ``ca-certificates`` package:

.. code-block:: bash

    apt-get install ca-certificates


Now you can download the script which will install docker and docker compose.

.. code-block:: bash

    wget https://gitlab.com/xivoxc/packaging/raw/2016.03/install/install-docker.sh -O install-docker.sh
    chmod +x install-docker.sh
    ./install-docker.sh


Install ntp server
------------------

.. note:: To be run on the XiVO CC server

The XiVO CC server and the XiVO PBX server must be synchronized to the same NTP source.

.. code-block:: bash

    apt-get install ntp

Recomended configuration : you should configure the NTP server of the XiVO CC server towards the XiVO PBX.
In our example it means to add the following line in the file `/etc/ntp.conf`::

  server 192.168.0.1 iburst



Install XiVO CC
---------------

.. note:: To be run on the XiVO CC server

This step will install the XiVO CC components via the ``xivocc-installer`` package. It is required to restart XiVO PBX during or after the setup process.
The installer will ask whether you wish to restart XiVO PBX later.

 .. warning::
  * This package must be installed on the XivoCC server.
  * Wizard **MUST** be passed on the XiVO PBX.
  * XiVO PBX services will need to be restarted.
    The installer will ask whether you wish to restart XiVO PBX during or after the setup process.


Also, check that you have following information:

     * XiVO PBX root password;
     * OpenSSH ``PermitRootLogin`` set to ``yes`` (you could revert to ``no`` after installation of XivoCC);
     * XiVO PBX's IP address;
     * XiVO CC IP address (the one visible *by* XiVO PBX);
     * Number of weeks to keep statistics;
     * Number of weeks to keep recordings (beware of space disk);

Install the *xivocc-installer* package via *apt*.

.. code-block:: bash

    echo "deb http://mirror.xivo.solutions/archive/ xivo-solutions-2016.03 main" > /etc/apt/sources.list.d/xivo-solutions.list
    wget http://mirror.xivo.solutions/xivo_current.key -O - | apt-key add -
    apt-get update
    apt-get install xivocc-installer


Install the recording
---------------------

.. note:: To be run on the **XiVO PBX** server

To be able to install the package you must have the XiVO Solutions repository on your **XiVO PBX**.
Then install on the **XiVO PBX** the debian package available in the repository.

.. code-block:: bash

    apt-get install xivo-recording

During the installation, you will be asked for :

- the recording server IP (192.168.0.2)
- the XiVO name (it must not contain any space or "-" character).

If you have more than one XiVO, you must give a different name to each of them.

This package installs two dialplan subroutines :

- xivo-incall-recording : used to record incoming calls
- xivo-outcall-recording : used to record outgoing calls

To use the subroutines, you must edit the configuration file :file:`/etc/xivo/asterisk/xivo_globals.conf`
and assign them to chosen preprocess subroutines. E.g. :

.. code-block:: bash

    XIVO_PRESUBR_FWD_QUEUE = xivo-incall-recording
    ...
    XIVO_PRESUBR_GLOBAL_QUEUE = xivo-incall-recording

If you want to record on a gateway used with Xivo, you must not use the xivo-recording package but gateway-recording.

If you want to use call recording filtering, please install also:

.. code-block:: bash

    apt-get install call-recording-filtering

During the installation, you will be asked for :

- the recording server address with protocol and port (\http://192.168.0.2:9400)


Using the XivoCC configuration manager : http://192.168.0.2:9100/ add user xuc as administrator to be able to get call history in web assistant.

After-install steps
-------------------

After the successful installation, start docker containers by an alias which was added to ~/.bashrc

.. code-block:: bash

    source ~/.bashrc
    dcomp up -d

If you selected to restart XiVO PBX later, please do so when possible to apply the modifications made by the installer.
The XUC server will not be able to connect correctly to the database on XiVO PBX.

To restart XiVO services, on XiVO PBX server run

.. code-block:: bash

    xivo-service restart all

Reinstallation
--------------

To reinstall the package, it is required to *apt-get purge xivocc-installer* followed by *apt-get install xivocc-installer*. This will re-run the configuration
of the package, download the docker compose template and setup XiVO PBX.

Purging the package will also **remove** the *xuc* and *stats* users from the XiVO PBX database.

Known Issues
------------

To avoid problems when uninstalling, you should:
    * to uninstall, please use ``apt-get purge xivocc-installer``
    * if the process is aborted, it will break the installation, please ``apt-get purge`` and ``apt-get install`` again


Checking Installed Version
--------------------------

Compoment version can be find in the log files, on the web pages for web components. You may also get the version from the docker container itself by typing :

.. code-block:: bash

    docker exec -ti xivocc_xucmgt_1 cat /opt/docker/conf/appli.version

Change xivocc_xucmgt_1 by the component version you want to check

Using XivoCC
------------

The various applications are available on the following addresses:

.. figure:: fingerboard.png
   :scale: 100%


- Xuc-related applications: http://192.168.0.2:8070/
- SpagoBI: http://192.168.0.2:9500/
- Config Management: http://192.168.0.2:9100/
- Recording server: http://192.168.0.2:9400/
- Kibana: http://192.168.0.2/

Post Installation
=================

User Configuration
------------------

* Using the configuration manager : http://192.168.0.2:9100/ (default user avencall/superpass)  add a user to be able to use the recording interface with proper rights.

.. note::
  Xuc server default user is xuc, add xuc as administrator to be able to get call history in web assistant.

.. warning::
  If you change the cti login username in xivo configuration, user has to be recreated with apropriate rights in configuration manager.

SpagoBi
-------

- Go to http://192.168.0.2:9500/SpagoBI (by default login: biadmin, password: biadmin)
- Update default language : go to "⚙ Resources" > "Configuration management" > in the "Select Category" field, chose "LANGUAGE_SUPPORTED" and change value of the label "SPAGOBI.LANGUAGE_SUPPORTED.LANGUAGE.default" in your language : fr,FR , en,US , ...
- Download the standard reports from https://gitlab.com/xivocc/sample_reports/raw/master/spagobi/standardreports.zip
- Import zip file in SpagoBI: " Repository Management" > Click on "Browse" and choose the previous downloaded zip file > Click on "Import" (All default options, with Jasper Report Engine as Engine associations).

XivoCC Default Report Sample
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: reportsample.png
    :scale: 50%


Use the database status report to check if replication and reporting generation is working :

.. figure:: datastatus.png
   :scale: 100%


ACD outgoing calls
------------------

XivoCC agent can make outgoing calls through an outgoing queue. This brings the statistics and supervision visualization for outgoing ACD calls. However, some special configuration steps are required:

- You need to create an outgoing queue with a name starting with 'out', e.g. outgoing_queue.
- This queue must be configured with preprocess subroutine xuc_outcall_acd, without On-Hold Music (tab General), Ringing Time must be 0 and Ring instead of On-Hold Music must be activated (both tab Application).
- The subroutine must be deployed on the Xivo server (to /etc/asterisk/extension_extra.d/ or through the web interface), the file is available from https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/xuc_outcall_acd.conf, with owner asterisk:www-data and rights 660.
- You must also deploy the file https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/generate_outcall_skills.py to /usr/local/sbin/, with owner root:root and rights 755.
- Furthermore, you must replace the file /etc/asterisk/queueskills.conf by the following one https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/queueskills.conf (be sure to backup the original one), without changing the owner or rights
- And finally you need to add a new skill rule on the Xivo server: Services -> Call center -> Skill rules -> Add, with name 'select_agent' and rules '$agent > 0'.

Once done, calls requested by an agent through the Cti.js with more than 6 digits are routed via the outgoing queue. You can change the number of digits using the parameter xuc.outboundLength in the xuc's configuration.

Totem Panels
------------

Data replication can take some time if there are a lot of data in xivo cel and queue log tables. You may check xivo-db-replication log files (/var/log/xivocc/xivo-db-replication.log).

Preconfigured panels are available on  http://@IP/kibana/#/dashboard/file/queues.json et http://@IP/kibana/#/dashboard/file/agents.json to be able to save this panels in elasticsearch database you have
to sign on on request user admin/Kibana

.. figure:: totempanel.png
    :scale: 50%

.. _check-list:

Post Installation Check List
----------------------------

- All components are running : dcomp ps
- Xuc internal database is synchronized with xivo check status page with http://xivoccserver:8090/
- CCManager is running, log a user and check if you can see and manage queues : http://xivoccserver:8070/ccmanager
- Web agent is running, log an agent and check if you can change the status : http://xivoccserver:8070/agent
- Web assistant is running, and you get call history : http://xivoccserver:8070/
- Check database replication status using spagobi system report
- Check elasticsearch database status (totem panels) http://xivoccserver:9200/queuelogs/_status
- Check that you can listen to recordings http://xivoccserver:9400/
- Check totem panels http://192.168.85.102/kibana

###### reminder: Make sure to have few calls made in your XiVO, despite you will not see **anything** in totem or spagobi.

Ldap Authentication
===================

Xuc
---

Configure LDAP authent for CCmanager, Web Assistant and Web Agent

You need to include in the compose.yml file a link to a specific configuration file by adding in xuc section a specific volume
and an environment variable to specify the alternate config file location

::

   xuc:

   ....

   environment:
   ....
   - CONFIG_FILE=/conf/xuc.conf

   volumes:
   - /etc/docker/xuc:/conf


Edit in /etc/docker/xuc/ a configuration file named xuc.conf to add ldap configuration (empty by default)


::

   include "application.conf"

   authentication {
     ldap {
       managerDN = "uid=company,ou=people,dc=company,dc=com"      # user with read rights on the whole LDAP
       managerPassword = "xxxxxxxxxx"                             # password for this user
       url = "ldap://ldap.company.com:389"                        # ldap URI
       searchBase = "ou=people,dc=company,dc=com"                 # ldap entry to use as search base
       userSearchFilter = "uid=%s"                                # filter to use to search users by login, using a string pattern
     }
   }

Recreate the container : `dcomp up -d xuc`
