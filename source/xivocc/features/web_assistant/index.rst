*************
Web Assistant
*************

The Web Assistant enables a user to:

* search contacts, 
* call them, 
* manage its favorites,
* manage its forward.


Login
=====

To login, one must have a user configured on the *XiVO PBX* with:

* XiVO Client enabled,
* Login, password and profile configured


Search
======

You can use the search section to lookup for people in the company:

.. figure:: search.png
   :scale: 100%

For this to work, one must configure the directories in the *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.


.. note:: Integration note: the *Web Assistant* support only the display of
  
  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email


Favorites
=========

One can clic on the star to put a contact in its list of favorites.

For this to work, favorites must be configured in the *XiVO PBX* as described in :ref:`dird-favorites-configuration`.

Phone integration
=================

The *Web Assistant* can integrate with the phone to :

* call,
* put on hold,
* transfer
* etc.

For this feature to work one must use a :ref:`phone_integration_support` and follow the :ref:`phone_integration_installation` page.

WebRtc integration
==================

The *Web Assistant* can be used by users with WebRtc configuration, without physical phone. It requires:

* microphone and headphones
* configure user with WebRtc line - see: :ref:`configure_user_with_webrtc_line`
* use Google Chrome browser version 55 (tested on 55.0.2883.87 m 64-bit)
* open *Web Assistant* using secure `https://` protocol (instead of `http://`)

.. note:: Currently you can not have user configured with both phone and WebRtc configuration at the same time.

