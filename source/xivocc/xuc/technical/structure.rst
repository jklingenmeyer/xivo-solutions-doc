******************************
Technical structure of XiVO-CC
******************************

Reporting
=========

The reporting is composed of four packages: pack-reporting, xivo-full-stats, xivo-reporting-db and xivo-db replication.

These packages will feed the tables of the xivo_stats database:

- xivo-db-replication feeds the tables cel and queue_log in real time, and the configuration tables (dialaction, linefeatures, etc...) every 5 minutes
- xivo-full-stats feeds in real time tha tables call_on_queue, call_data, stat_queue_periodic, stat_agent_periodic and agent_position
- xivo-reporting-db and pack-reporting work together to feed the tables stat_queue_specific, stat_agent_queue_specific and stat_agent_specific every 15 minutes